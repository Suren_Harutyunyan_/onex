import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})

export class AuthGuard implements CanActivate {

  constructor(private router: Router) {
  }

  canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean | Observable<boolean> | Promise<boolean> | boolean {
    if (localStorage.getItem('user')) {
      if (state.url.indexOf('/login') >= 0 || state.url.indexOf('/registration') >= 0) {
        this.router.navigate(['/dashboard']);
        return false;
      } else {
        return true;
      }
    } else {
      if (state.url.indexOf('/login') >= 0 || state.url.indexOf('/registration') >= 0) {
        return true;
      } else {
        this.router.navigate(['/login']);
        return false;
      }
    }
  }
}
