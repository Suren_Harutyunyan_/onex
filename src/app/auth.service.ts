import {Injectable} from '@angular/core';
import {Router} from '@angular/router';
import {UsersService} from './users.service';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  constructor(private router: Router, private usersService: UsersService) {
  }

  // checking is user on array of users
  public login(user) {
    if (!localStorage.getItem('users')) {
      return false;
    } else {
      const users = this.usersService.getUsers();
      const ifUser = users.filter(obj => {
        return obj.email === user.email && obj.pass === user.pass;
      });
      if (ifUser && ifUser.length > 0) {
        localStorage.setItem('user', JSON.stringify(ifUser[0]));
        return !!localStorage.getItem('user');
      } else {
        return false;
      }
    }
  }

// simple logout function
  public logOut() {
    localStorage.removeItem('user');
    this.router.navigate(['/login']);
  }
}
