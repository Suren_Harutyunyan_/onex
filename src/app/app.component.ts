import {Component, HostListener} from '@angular/core';
import {AuthService} from './auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  /*@HostListener('window:unload', [ '$event' ])
  unloadHandler(event) {
    this.authService.logOut()
  }*/
  constructor(private authService: AuthService) {

  }
}
