export interface User {
  email: string;
  pass: string;
}

export interface Users {
  firstname: string;
  lastname: string;
  city: string;
  workplace: string;
  email: string;
  pass: string;
  id?: string;
}
