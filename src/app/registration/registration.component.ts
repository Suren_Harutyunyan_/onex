import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {AuthService} from '../auth.service';
import {Users} from '../user';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})
export class RegistrationComponent implements OnInit {
  protected userInfo: Users;
  public error: boolean;

  constructor(private router: Router) {
  }

  ngOnInit() {
    this.error = false;
    this.userInfo = {
      firstname: '',
      lastname: '',
      city: '',
      workplace: '',
      email: '',
      pass: '',
      id: ''
    };
  }

  // simple sign up method with email validation and  localStorage
  signUp() {
    const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (!this.userInfo.email || !re.test(String(this.userInfo.email).toLowerCase())) {
      this.error = true;
    } else {
      this.userInfo.id = JSON.stringify(+new Date());
      let users = JSON.parse(localStorage.getItem('users'));
      if (users && users.length > 0) {
        users.push(this.userInfo);
        localStorage.setItem('users', JSON.stringify(users));
        this.router.navigate(['/login']);
      } else {
        users = [];
        users.push(this.userInfo);
        localStorage.setItem('users', JSON.stringify(users));
        this.router.navigate(['/login']);
      }
    }

  }
}
