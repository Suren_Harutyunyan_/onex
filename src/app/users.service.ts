import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  constructor() { }

  public getUsers() {
    const users = JSON.parse(localStorage.getItem('users'));
    return users;
  }
}
