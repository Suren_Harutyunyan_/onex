import {Component, HostListener, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {AuthService} from '../auth.service';
import {UsersService} from '../users.service';
import {Users} from '../user';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {
  public user: Users;

  constructor(private activeRouter: ActivatedRoute, private usersService: UsersService) {
  }

  ngOnInit() {
    this.activeRouter.params.subscribe((value: {id: string}) => {
      const users = this.usersService.getUsers();
      this.user = users.filter(obj => {
        return obj.id === value.id;
      })[0];
    });
  }

}
