import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {AuthService} from '../auth.service';
import {UsersService} from '../users.service';
import {PagingService} from '../paging.service';
import {Users} from '../user';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  public users: [Users];
  public pager: any = {};

  // paged items
  public pagedItems: any[];

  constructor(private authService: AuthService, private usersService: UsersService, private pagingService: PagingService) {
  }

  ngOnInit() {
    this.users = this.usersService.getUsers();
    this.setPage(1);
  }

  public setPage(page: number) {
    // get pager object from service
    this.pager = this.pagingService.getPager(this.users.length, page);
    // get current page of items
    this.pagedItems = this.users.slice(this.pager.startIndex, this.pager.endIndex + 1);
  }

}
