import {Component, OnInit} from '@angular/core';
import {AuthService} from '../auth.service';
import {Router} from '@angular/router';
import {User} from '../user';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  public user: User;
  public error: boolean;
  public errorMessage: string;

  constructor(private authService: AuthService, private router: Router) {
  }

  ngOnInit() {
    this.error = false;
    this.user = {
      email: '',
      pass: ''
    };
  }

  protected login() {
    // validating email and password before login
    const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (!this.user.email || !re.test(String(this.user.email).toLowerCase())) {
      this.errorMessage = 'Email is required or not valid';
      this.error = true;
    } else if (!this.user.pass) {
      this.errorMessage = 'Password is required';
    } else {
      // getting from authService boolean value
      if (this.authService.login(this.user)) {
        this.router.navigate(['dashboard']);
      } else {
        this.errorMessage = 'Invalid credentials';
        this.error = true;
      }
    }

  }
}
